# iGotcha Media Code Challenge

This test should be completed in 2-3 hours.


## Requirements

- Use the following cocktail recipe API to design an application: (https://www.thecocktaildb.com/api.php).
- Design an application with a search function to search recipes by name or by ingredient.
- When user types more than 3 characters, the search result should show while user is typing

![mockup](./mockup.png "Mockup")


## Languages & Library

- Ruby or Javascript(node.js)
- Vue


## Evaluation

- Solution correctness
- Application should be responsive
- Developer experience (how easy it is to run your solution locally)
- Code style and cleanliness
- Performance
- Short explanation paragraph: please briefly explain the thought process behind your work.

#### Bonus
  - Document your code
  - Ability to make sensible assumptions - We know that the test is short, it's not enough time to develop a product ready for production. Explain to us what you would do if you have more time.